Gala
====

Projet Symfony pour le site du gala à la place du WordPress actuel.

Dev
===

1. Installer les dépendances : `php composer install`
2. Éditer son fichier `parameters.yml`
3. Lancer le serveur php : `php bin/console server:run`
4. Visiter http://127.0.0.1:8000

Prod
====

1. Commit sur master
2. Deploy with Gitlab (manual job)
