<?php

namespace Gala\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class PhotosUpload
{
  /**
   * @var ArrayCollection
   */
  protected $files;
  
  public function getFiles() {
    return $this->files;
  }

  public function setFiles($files) {
    $this->files = $files;

    return $this;
  }

  public function addFile($file) {
    $this->files[] = $file;

    return $this;
  }
}
