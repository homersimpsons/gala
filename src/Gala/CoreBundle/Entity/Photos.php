<?php

namespace Gala\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Photos
 *
 * @ORM\Table(name="photos")
 * @ORM\Entity(repositoryClass="Gala\CoreBundle\Repository\PhotosRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Photos
{
    /**
     * @Assert\Image(
     *  maxSize = "5M",
     * )
     */
    protected $file;
    
    private $tempFileName;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=4)
     */
    private $extension;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return string
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    public function getFileNameExt()
    {
        return $this->getId().'.'.$this->getExtension();
    }

    public function setFile(UploadedFile $file)
	{
		$this->file = $file;
		if (null !== $this->getId()) {
			$this->tempFileName = $this->getId();
			$this->extension = null;
		}
    }
    
	public function getFile()
	{
		return $this->file;
	}
    
    /**
	* @ORM\PrePersist()
	* @ORM\PreUpdate()
	*/
	public function preUpload()
	{
		if (null === $this->file) {
			return;
		}
        $this->setExtension($this->file->guessExtension());
        $this->preUpdateFile();
	}

    protected function preUpdateFile(){} // To define if specific
	
	/**
	 * @ORM\PrePersist()
	 */
	public function prePersistDate()
	{
		$this->date = new \DateTime();
		return $this;
	}
    
	/**
	* @ORM\PostPersist()
	* @ORM\PostUpdate()
	*/
	public function upload()
	{
		if (null === $this->file) {
			return;
		}

		// Si on avait un ancien fichier, on le supprime
		if (null !== $this->tempFileName) {
			$oldFile = $this->getUploadRootDir().$this->tempFileName;
			if (file_exists($oldFile)) {
				unlink($oldFile);
			}
		}

		// On déplace le fichier envoyé dans le répertoire de notre choix
		$this->file = $this->file->move(
			$this->getUploadRootDir(), // Le répertoire de destination
			$this->getFileNameExt()   // Le nom du fichier à créer
        );
		
		$this->postUpdateFile();
    }

    protected function postUpdateFile(){} // To define if specific treatment

    /**
    * @ORM\PreRemove()
    */
    public function preRemoveUpload()
    {
        // On sauvegarde temporairement le nom du fichier
        $this->tempFileName = $this->getFileNameExt();
        $this->preRemoveFile();
    }
    
    protected function preRemoveFile(){} // To define if specific treatment

	/**
	* @ORM\PostRemove()
	*/
	public function removeUpload()
	{
        $oldFile = $this->getUploadRootDir().$this->tempFileName;
        if (file_exists($oldFile)) {
            unlink($oldFile);
        }
        $this->postRemoveFile();
    }
    
    protected function postRemoveFile(){} // To define if specific treatment

    public function getFileUri()
    {
        return $this->getUploadDir().$this->getFileNameExt();
    }

	public function getUploadDir()
	{
		// On retourne le chemin relatif vers l'image pour un navigateur
		return 'uploads/';
	}

	protected function getUploadRootDir()
	{
		// On retourne le chemin relatif vers l'image pour notre code PHP
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    public function __toString() {
        return $this->getFileNameExt();
    }
}
