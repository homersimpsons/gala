<?php
namespace Gala\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\All;

use Symfony\Component\OptionsResolver\OptionsResolver;

class PhotosType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('files', FileType::class, array(
					'multiple' => true,
					'constraints' => array(
						new All(array(
							'constraints' => new Image(),
						))
					),
				))
				->add('send', SubmitType::class);
	}

    public function getName()
    {
        return 'Photos';
    }
}
