<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Gala\CoreBundle\Form\DeleteType;
use Gala\CoreBundle\Form\PhotosType;
use Gala\CoreBundle\Entity\Photos;
use Gala\CoreBundle\Entity\PhotosUpload;

class PhotosController extends Controller
{
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();
      $photos = $em->getRepository('GalaCoreBundle:Photos')->findAll();
      return $this->render('GalaCoreBundle:Photos:index.html.twig', array(
        'photos' => $photos,
      ));
    }

    public function photoAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
      $photo = $em->getRepository('GalaCoreBundle:Photos')->find($id);
      
      if (null == $photo) {
        return $this->redirect($this->generateUrl('gala_photos'));
      }
      
      $options = array();
      $user = $this->getUser();
      if ($user) {
        $form = $this->get('form.factory')->create(DeleteType::class);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
          $em->remove($photo);
          $em->flush();
          return $this->redirect($this->generateUrl('gala_photos'));
        }
        $options['form'] = $form->createView();
      }
      $options['photo'] = $photo;
      return $this->render('GalaCoreBundle:Photos:photo.html.twig', $options);
    }

    public function uploadAction(Request $request)
    {
      $photos = new PhotosUpload();
      $form = $this->get('form.factory')->create(PhotosType::class, $photos);
			if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
				$em = $this->getDoctrine()->getManager();
        foreach ($photos->getFiles() as $file) {
          $photo = new Photos();
          $photo->setFile($file);
          $em->persist($photo);
        }
				$em->flush();
				
				$request->getSession()->getFlashBag()->add('info', 'Image enregistrée.');
				
				return $this->redirect($request->getUri());
			}
      return $this->render('GalaCoreBundle:Photos:upload.html.twig', array(
        'form' => $form->createView(),
      ));
    }
}
