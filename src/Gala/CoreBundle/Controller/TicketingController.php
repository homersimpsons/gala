<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TicketingController extends Controller
{
    public function indexAction()
    {
        return $this->render('GalaCoreBundle:Ticketing:index.html.twig', array(
            'open' => true
        ));
    }
}
