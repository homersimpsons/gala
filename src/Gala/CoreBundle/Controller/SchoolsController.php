<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SchoolsController extends Controller
{
    public function indexAction()
    {
        return $this->render('GalaCoreBundle:Schools:index.html.twig');
    }
}
