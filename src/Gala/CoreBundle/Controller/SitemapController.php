<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends Controller
{
    public function indexAction()
    {
      $urls = array();
      $urls[] = array('loc' => $this->generateUrl('gala_homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_photos', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_ticketing', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_schools', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_partners', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_access_accomodation', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $urls[] = array('loc' => $this->generateUrl('gala_contact', array(), UrlGeneratorInterface::ABSOLUTE_URL));
      $em = $this->getDoctrine()->getManager();
      $photos = $em->getRepository('GalaCoreBundle:Photos')->findAll();
      foreach ($photos as $photo) {
        $urls[] = array('loc' => $this->generateUrl('gala_photo', array('id' => $photo->getId()), UrlGeneratorInterface::ABSOLUTE_URL));
      }
      $response = new Response();
      $response->headers->set('Content-Type', 'application/xml');
      return $this->render('GalaCoreBundle::sitemap.xml.twig',
        array('urls' => $urls),
        $response
      );
    }
}
