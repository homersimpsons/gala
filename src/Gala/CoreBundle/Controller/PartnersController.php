<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PartnersController extends Controller
{
    public function indexAction()
    {
        return $this->render('GalaCoreBundle:Partners:index.html.twig');
    }
}
