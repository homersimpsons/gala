<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $winners = array(
            array('lot' => '1 Voyage dans un pays européen', 'name' => 'charmantray raymond'),
            array('lot' => '1 tablette numérique', 'name' => 'Arnaud Emmanuel'),
            array('lot' => '1 enceinte bluetooth', 'name' => 'Wissart Rémi '),
            array('lot' => '1 panier chocolat', 'name' => 'Aubert Rodolphe'),
            array('lot' => '1 panier chocolat', 'name' => 'Scalbert Cécile '),
            array('lot' => '1 toile Gala + set de 6 porte-serviettes', 'name' => 'Jules Claussen'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Clémence Bras'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Oleron Florian'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Fortuné Étienne '),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Martineau Patricia'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Lecompte'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Patte Cecile'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Brodin Aurore'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Kalinowski Clement'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Masse Elisabeth'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Lecompte Michel'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Bourin Aude'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Pierre de la Brière Marianne'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Le Bars Vincent'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Rappasse Clement'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Belland Amandine'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Petit Lucas'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Duquesnoy Claire'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Perdrix Esperanza'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Vo Alida'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Hazard Catherine'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Chevallier Raymond'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Delaby Jacqueline'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Devigne Thibaut'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Marbaix Michel'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Joly Lisa'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Lemonnier Carole'),
            array('lot' => '1 affiches Gala A2 + 2 gobelets + set de 6 porte-serviettes', 'name' => 'Coignet Anthony'),
            array('lot' => '3 gobelets', 'name' => 'Zultuto David'),
            array('lot' => '3 gobelets', 'name' => 'Wetteren Alice'),
            array('lot' => '3 gobelets', 'name' => 'Camminada Mathieu'),
            array('lot' => '3 gobelets', 'name' => 'Crépieux Laurent'),
            array('lot' => '3 gobelets', 'name' => 'Contreras José'),
            array('lot' => '3 gobelets', 'name' => 'Kerckhove Alice'),
            array('lot' => '3 gobelets', 'name' => 'Gaillard Annie'),
            array('lot' => '3 gobelets', 'name' => 'Hallereau Estevan'),
        );
        return $this->render('GalaCoreBundle:Default:index.html.twig', array(
            'winners' => $winners,
        ));
    }
}
