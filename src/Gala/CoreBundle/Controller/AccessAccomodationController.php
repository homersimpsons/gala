<?php

namespace Gala\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccessAccomodationController extends Controller
{
    public function indexAction()
    {
        return $this->render('GalaCoreBundle:AccessAccomodation:index.html.twig');
    }
}
